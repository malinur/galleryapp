import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

export default function Card({title, subtitle, imageUrl, onPress}) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.card}>
        <Image style={styles.image} source={{uri: imageUrl}} />
        <View style={styles.detailsContainer}>
          <Text style={styles.title} numberOfLines={2}>
            {title}
          </Text>
          <Text style={styles.subtitle} numberOfLines={1}>
            {subtitle}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginBottom: 20,
    overflow: 'hidden',
    flexDirection: 'row',
  },
  detailsContainer: {
    padding: 10,
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
  },
  subtitle: {
    color: '#808080',
  },
  title: {
    color: '#363636',
    marginBottom: 7,
    flexShrink: 1,
    fontWeight: 'bold',
  },
});
