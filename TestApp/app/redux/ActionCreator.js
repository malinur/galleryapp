import axios from 'axios';
import {fetchDataPending, fetchDataSuccess, fetchDataError} from './Action';

const ActionCreator = (url) => (dispatch) => {
  return new Promise(() => {
    axios
      .get(url)
      .then((response) => {
        dispatch(fetchDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchDataError(error));
      });
  });
};

export default ActionCreator;
