import ACTION_TYPES from './ActionTypes.js';

export const fetchDataPending = () => ({
  type: ACTION_TYPES.API_PENDING,
});

export const fetchDataSuccess = (data) => ({
  type: ACTION_TYPES.API_SUCCESS,
  payload: data,
});

export const fetchDataError = (error) => ({
  type: ACTION_TYPES.API_ERROR,
  payload: error,
});
