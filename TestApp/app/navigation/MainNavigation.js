import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ListingsScreen from '../screens/ListingsScreen';
import ViewImageScreen from '../screens/ViewImageScreen';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

const MainNavigation = () => (
  <NavigationContainer
    screenOptions={{
      gestureEnabled: true,
    }}>
    <Stack.Navigator>
      <Stack.Screen name="Photos" component={ListingsScreen} />
      <Stack.Screen name="Photo" component={ViewImageScreen} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default MainNavigation;
