import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';

import apiReducer from '../redux/Reducer';
import ActionCreator from '../redux/ActionCreator';

import Card from '../components/Card';

export default class ListingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listings: [],
    };
  }

  async componentDidMount() {
    const response = await fetch(
      'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0',
    );
    const data = await response.json();
    this.setState({listings: data});
    console.log(data);
  }

  render() {
    const {listings} = this.state;

    return (
      <FlatList
        style={styles.screen}
        data={listings}
        keyExtractor={(listing) => listing.id}
        renderItem={({item: listing}) => (
          <Card
            title={listing.alt_description}
            subtitle={listing.user.username}
            imageUrl={listing.urls.thumb}
            onPress={() => this.props.navigation.navigate('Photo', listing)}
          />
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: '#f8f4f4',
  },
});

// const mapStateToProps = (state) => ({
//   data: state.apiReducer.data,
//   error: state.apiReducer.error,
// });

// const mapDispatchToProps = (dispatch) => ({
//   ActionCreator: (url) => dispatch(ActionCreator(url)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(ListingsScreen);
