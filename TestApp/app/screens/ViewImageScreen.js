import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

export default function ViewImageScreen({route}) {
  const listing = route.params;

  return (
    <View style={styles.container}>
      <Image
        resizeMode="contain"
        style={styles.image}
        source={{uri: listing.urls.regular}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    width: '100%',
    height: '100%',
  },
});
