import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';

import MainNavigation from './app/navigation/MainNavigation';
import Store from './app/redux/Store';

export default function App() {
  return (
    <Provider store={Store}>
      <MainNavigation />
    </Provider>
  );
}
